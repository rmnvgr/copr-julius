%define exec julius
%define uuid com.github.bvschaik.julius

Name:			julius-game
Version:		1.3.1
Release:		1%{?dist}
Summary:		An open source re-implementation of Caesar III 
License:		AGPL-3.0
URL:			https://github.com/bvschaik/julius
Source0:		https://github.com/bvschaik/julius/archive/v%{version}.tar.gz

BuildRequires:	cmake
BuildRequires:	gcc-c++
BuildRequires:	SDL2-devel >= 2.0
BuildRequires:	SDL2_mixer-devel >= 2.0

%description
Julius is an open source re-implementation of Caesar III.

The aim of this project is to create an open-source version of Caesar 3, with the same logic as the original, but with some UI enhancements, that is able to be played on multiple platforms. The same logic means that the saved games are 100% compatible, and any gameplay bugs present in the original Caesar 3 game will also be present in Julius.

UI enhancements include:
- Support for widescreen resolutions
- Windowed mode support for 32-bit desktops

Julius requires the original assets (graphics, sounds, etc) from Caesar 3 to run.

%prep
%setup -n %{exec}-%{version}

%build
%cmake .
%make_build

%install
%make_install

%files
%{_bindir}/%{exec}
%{_datadir}/applications/%{uuid}.desktop
%{_datadir}/metainfo/%{uuid}.metainfo.xml
%{_datadir}/icons/hicolor/*/apps/%{uuid}.png

%changelog
* Mon Feb 24 2020 - 1.3.1-1
- Upstream 1.3.1
* Fri Feb 14 2020 - 1.3.0-1
- Upstream 1.3.0
* Wed Jan 08 2020 - 1.2.0-1
- Upstream 1.2.0
- Use upstream icons and desktop file
* Sun May 26 2019 - 1.1.0-1
- Upstream 1.1.0
* Fri Dec 28 2018 - 1.0.0-1
- Initial spec file

